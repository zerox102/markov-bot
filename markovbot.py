import keyring
import discord
import markovify

APIKEY = keyring.get_password("api_key_bot", "username")
channels = dict()

class DClient(discord.Client):
    async def on_ready(self):
        print('Logged on as {0}!'.format(self.user))
    
    async def on_message(self, message):
        if(message.content.startswith("nya~")):
            if(message.content.startswith("nya~markovRead")):
                await read_channel(self, message)

            if(message.content.startswith("nya~markovCreate")):
                await create_markov(self, message)
        

async def create_markov(client:DClient, cmdMessage:discord.Message):
    #check syntax
    args = cmdMessage.content.split(" ")

    if(args[0] != "nya~markovCreate"):
        return

    if(len(args) != 3):
        await cmdMessage.channel.send("Sorry, the syntax for this should be 'nya~markovCreate [ChannelID] [Username]'")
        return

    if not (args[1] in channels):
        await cmdMessage.channel.send("That channel has not been indexed yet. Call 'nya~markovRead [ChannelID] on it first.")
        return

    if not (args[2] in channels[args[1]]):
        await cmdMessage.channel.send("The user does not appear to have any indexed messages for this channel.")
        return    

    try:
        usertext = ""
        for text in channels[args[1]][args[2]]:
            usertext += text + "\n"
        usertext = usertext[:-1]

        model = markovify.NewlineText(usertext)
        sentence = model.make_sentence(tries=100)
        if sentence != None: 
            await cmdMessage.channel.send("```" + sentence + "```")
        else:
            await cmdMessage.channel.send("The user does not appear to have enough messages to build a markov sentence from this channel.")
    except Exception as e:
        await cmdMessage.channel.send("Encountered an error while attempting to build the message...")
        print(e)

async def read_channel(client:DClient, cmdMessage:discord.Message):
    #check syntax
    args = cmdMessage.content.split(" ")

    if(args[0] != "nya~markovRead"):
        return

    if(len(args) != 2):
        await cmdMessage.channel.send("Sorry, the syntax for this should be 'nya~markovRead [ChannelID]'")
        return

    if(args[1] in channels):
        await cmdMessage.channel.send("That channel has already been indexed.")
        return

    try:
        await cmdMessage.channel.send("Okay, attempting to index messages from this channel...")
        dictionary = dict() #dictionary of author: list of messages
        channel = await client.fetch_channel(args[1]) #get channel
        messages = await channel.history(limit=7500).flatten() #load the last 10000 messages
        
        for msg in messages: #sort messages into a dictionary by author
            if msg.content != '':
                if msg.author.name not in dictionary:
                    dictionary[msg.author.name] = [msg.content]
                else:
                    dictionary[msg.author.name].append(msg.content)

        channels[args[1]] = dictionary #add dictionary to new dictionary with channelID as the key
        await cmdMessage.channel.send("Channel indexed successfully.")
    except Exception as e:
        await cmdMessage.channel.send("Encountered an error while indexing this channel...")
        print(e)


client = DClient()
client.run(APIKEY)
